# DS-EXPLORER
Elasticsearch data source explorer.  
This project is a part of the data source joiner application bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.  

### Description
DS-EXPLORER is a stand-alone Spring-Boot web application that exposes a simple RESTfull API to  
perform full-text searches against existing data sources persisted in Elasticsearch.  

### Installation
The simplest way to start using the application is to run it in a bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.  
However, it can also be run in a stand-alone mode from IDE (IntelliJ), "fat" jar, or in a docker container.

#### Stand-alone mode

##### Prerequisites

* Running Elasticsearch instance.

##### Configuration

Modify properties in [src/main/resources/](src/main/resources/) to reflect your own settings.  
Update [elasticsearch.properties](src/main/resources/elasticsearch.properties)  
```yml
    es.host = <elasticsearch-host-name>
    es.port = 9200
    es.connection-timeout = 5000
```  

##### Build and run

From the project's root:

- Generate jar with dependencies and run:  
```bash
    ./gradlew clean build
    java -jar build/libs/<jar-name>
```  
- Or, build a docker container and run:  
```bash
    ./gradlew clean build
    docker build -t ds-explorer .
    docker run ds-explorer:latest -p 8084:8084
```  

DS-EXPLORER API will be available at: http://hostname:8084  

### REST API usage and examples

#### Get elasticsearch cluster info

##### Request:
```bash
    curl http://localhost:8084/api/v1/es/cluster/info
```  

##### Response:  
```json
    {
        "name" : "UBnyOzt",
        "cluster_name" : "joiner-es",
        "cluster_uuid" : "NCW3DJkaS1GXW4MyqfUC7Q",
        "version" : {
            "number" : "6.5.0",
            "build_flavor" : "default",
            "build_type" : "tar",
            "build_hash" : "816e6f6",
            "build_date" : "2018-11-09T18:58:36.352602Z",
            "build_snapshot" : false,
            "lucene_version" : "7.5.0",
            "minimum_wire_compatibility_version" : "5.6.0",
            "minimum_index_compatibility_version" : "5.0.0"
        },
        "tagline" : "You Know, for Search"
    }
```  

#### Get a list of existing data sources

##### Request:
```bash
    curl http://localhost:8084/api/v1/es/ds/all
```  

##### Response:
```json
    [
        {
            "health": "yellow",
            "status": "open",
            "index": "ds5",
            "uuid": "FYrdt9kDTAO7ZZMycxaYTg",
            "pri": "5",
            "rep": "1",
            "docs.count": "300000",
            "docs.deleted": "0",
            "store.size": "35.7mb",
            "pri.store.size": "35.7mb"
        },
        {
            "health": "yellow",
            "status": "open",
            "index": "ds11",
            "uuid": "I9hwyPMBSsWPmqe4tewSCg",
            "pri": "5",
            "rep": "1",
            "docs.count": "468",
            "docs.deleted": "0",
            "store.size": "54.5kb",
            "pri.store.size": "54.5kb"
        }
    ]
``` 

#### Search existing data sources
Performs full-text searches against existing Elasticsearch data sources/indices.  

Supported search features:
* multi-index searches (multiple data sources)
* search by data source name, type, and/or field value (single value searches only)
* paging (page number and size)
* HATEOAS links

##### Request 1:
```bash
    curl http://localhost:8084/api/v1/es/ds/search?index=ds5&type=source&value=&page=5&size=2
```  
##### Response 1:  
```json
    {  
       "page":{  
          "page":5,
          "size":2,
          "totalDocuments":300000,
          "totalPages":150000,
          "links":[  
             {  
                "rel":"self",
                "href":"http://localhost:8084/api/v1/es/ds/search?index=ds5&type=source&value=&page=5&size=2"
             },
             {  
                "rel":"next",
                "href":"http://localhost:8084/api/v1/es/ds/search?index=ds5&type=source&value=&page=6&size=2"
             },
             {  
                "rel":"prev",
                "href":"http://localhost:8084/api/v1/es/ds/search?index=ds5&type=source&value=&page=4&size=2"
             }
          ]
       },
       "documents":[  
          {  
             "dsName":"ds5",
             "dsType":"source",
             "data":{  
                "ds5_col_1":"452179",
                "ds5_col_2":"jrKvS",
                "ds5_col_3":"YScqS"
             },
             "docId":"3_IUN2cBYwPW0er4XLMv"
          },
          {  
             "dsName":"ds5",
             "dsType":"source",
             "data":{  
                "ds5_col_1":"167193",
                "ds5_col_2":"GyavY",
                "ds5_col_3":"HaVrz"
             },
             "docId":"cvIUN2cBYwPW0er4XLiz"
          }
       ]
    }
```  
##### Request 2
```bash
    curl http://localhost:8084/api/v1/es/ds/search?value=jrKvS
```  

##### Response 2
```json
{  
   "page":{  
      "page":0,
      "size":2,
      "totalDocuments":1,
      "totalPages":0,
      "links":[  
         {  
            "rel":"self",
            "href":"http://localhost:8084/api/v1/es/ds/search?index=ds5&type=source&value=jrKvS&page=0&size=2"
         }
      ]
   },
   "documents":[  
      {  
         "dsName":"ds5",
         "dsType":"source",
         "data":{  
            "ds5_col_1":"452179",
            "ds5_col_2":"jrKvS",
            "ds5_col_3":"YScqS"
         },
         "docId":"3_IUN2cBYwPW0er4XLMv"
      }
   ]
}
```  




