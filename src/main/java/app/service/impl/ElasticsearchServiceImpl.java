package app.service.impl;

import app.controller.ElasticsearchController;
import app.dto.Document;
import app.dto.DocumentPage;
import app.dto.Page;
import app.dto.SearchCriteria;
import app.repository.ElasticsearchRepository;
import app.service.ElasticsearchService;
import app.utils.StreamUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.search.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class ElasticsearchServiceImpl implements ElasticsearchService {

    private Logger logger = LogManager.getLogger(ElasticsearchServiceImpl.class);

    @Autowired
    private ElasticsearchRepository repository;

    @Override
    public String getClusterInfo() throws IOException {
        return EntityUtils.toString(repository.getClusterInfo().getEntity());
    }

    @Override
    public String getExistingIndices() throws IOException {
        return EntityUtils.toString(repository.getAllIndices().getEntity());
    }

    @Override
    public DocumentPage search(SearchCriteria criteria) throws IOException, ElasticsearchException {
        SearchResponse response = repository.search(criteria);
        List<Document> documents = StreamUtils.asStream(response.getHits().iterator(), true)
                                              .map(hit -> new Document().setDockId(hit.getId())
                                                                            .setDsName(hit.getIndex())
                                                                            .setDsType(hit.getType())
                                                                            .setData(hit.getSourceAsMap()))
                                              .collect(Collectors.toList());

        logger.debug(new ObjectMapper().writeValueAsString(documents));

        long totalDocuments = response.getHits().totalHits;
        Page pagingInfo = new Page().setPage(criteria.getPage())
                                 .setSize(criteria.getSize())
                                 .setTotalDocuments(totalDocuments)
                                 .setTotalPages(totalDocuments/criteria.getSize());
        pagingInfo.add(getLinks(criteria, pagingInfo));
        return new DocumentPage().setDocuments(documents).setPage(pagingInfo);
    }

    private List<Link> getLinks(SearchCriteria criteria, Page page) {
        List<Link> links = new ArrayList<>();
        links.add(linkTo(methodOn(ElasticsearchController.class)
                                 .search(criteria.getIndex(),
                                         criteria.getType(),
                                         criteria.getValue(),
                                         page.getPage(),
                                         page.getSize())).withSelfRel());
        if (page.getPage() < page.getTotalPages()) {
            links.add(linkTo(methodOn(ElasticsearchController.class)
                                     .search(criteria.getIndex(),
                                             criteria.getType(),
                                             criteria.getValue(),
                                             page.getPage() + 1,
                                             page.getSize())).withRel("next"));
        }
        if (page.getPage() > 0) {
            links.add(linkTo(methodOn(ElasticsearchController.class)
                                     .search(criteria.getIndex(),
                                             criteria.getType(),
                                             criteria.getValue(),
                                             page.getPage() - 1,
                                             page.getSize())).withRel("prev"));
        }
        return links;
    }
}
