package app.service;


import app.dto.DocumentPage;
import app.dto.SearchCriteria;
import org.elasticsearch.ElasticsearchException;

import java.io.IOException;

public interface ElasticsearchService {

    /**
     * Retrieves and returns cluster info as JSON String
     * @return cluster info
     */
    String getClusterInfo() throws IOException;

    /**
     * Fetches and returns all existing cluster data sources with associated index statistics.
     * @return existing data sources (indices)
     */
    String getExistingIndices() throws IOException;

    /**
     * Returns a {@link DocumentPage} containing a list of documents
     * that meet specified {@link SearchCriteria}
     *
     * @param criteria search criteria
     * @return {@link DocumentPage}
     */
    DocumentPage search(SearchCriteria criteria) throws IOException, ElasticsearchException;
}
