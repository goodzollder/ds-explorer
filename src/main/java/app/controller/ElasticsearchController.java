package app.controller;

import app.dto.DocumentPage;
import app.dto.SearchCriteria;
import app.service.ElasticsearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.async.DeferredResult;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

@RestController
@RequestMapping(path = "api/v1/es")
public class ElasticsearchController {

    @Autowired
    private ExecutorService executor;

    @Autowired
    private ElasticsearchService esService;

    private Logger logger = LogManager.getLogger(ElasticsearchController.class);

    @GetMapping(path = "/cluster/info", produces = "application/json")
    public ResponseEntity getClusterInfo() throws IOException {
        logger.debug("Retrieving Elasticsearch cluster info");
        String response = esService.getClusterInfo();
        logger.debug("Cluster info: {}", response);
        return ResponseEntity.ok(response);

    }

    /**
     * Fetches and returns all existing Elasticsearch indices.
     * @return existing DS indices
     */
    @GetMapping(path = "/ds/all", produces = "application/json")
    public ResponseEntity getIndices() throws IOException {
        logger.debug("Retrieving existing indices.");
        return ResponseEntity.ok(esService.getExistingIndices());
    }

    /**
     * Exposes a non-blocking REST endpoint to perform searches against existing Elasticsearch data sources/indices.
     * Supported search features:
     *  - multi-index searches (multiple data sources)
     *  - search by data source name, type, and/or field value (single value searches only)
     *  - paging (page number and size)
     *
     * @param index - data source name
     * @param type - data source type (source/target)
     * @param value - field value to search for ()
     * @param page - desired page number (starts from 0)
     * @param size - desired page size
     *
     * @return {@link ResponseEntity} containing {@link DocumentPage}
     */
    @GetMapping(path = "/ds/search", produces = "application/json")
    public DeferredResult<ResponseEntity> search(@RequestParam(required = false, defaultValue = "") String index,
                                                 @RequestParam(required = false, defaultValue = "") String type,
                                                 @RequestParam(required = false, defaultValue = "") String value,
                                                 @RequestParam(required = false, defaultValue = "0") Integer page,
                                                 @RequestParam(required = false, defaultValue = "5") Integer size) {
        // retrieve attributes and re-apply them in the worker thread (HATEOAS issue)
        RequestAttributes attributes = RequestContextHolder.currentRequestAttributes();
        SearchCriteria criteria = new SearchCriteria().setIndex(index)
                                                      .setType(type)
                                                      .setValue(value)
                                                      .setPage(page)
                                                      .setSize(size);
        logger.debug("Searching with criteria: {}", criteria);
        DeferredResult<ResponseEntity> result = new DeferredResult<>();
        executor.submit(()-> {
            RequestContextHolder.setRequestAttributes(attributes);
            logger.debug("Processing search request in a separate thread");
            try {
                result.setResult(ResponseEntity.ok(esService.search(criteria)));
            } catch (Exception exception) {
                logger.debug("Exception occurred: {}", exception.getMessage());
                result.setErrorResult(exception);
            }
        });
        logger.debug("Releasing servlet thread.");
        return result;
    }
}
