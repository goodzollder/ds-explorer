package app.dto;

import java.util.Map;

public class Document {

    private String dockId;
    private String dsName;
    private String dsType;
    private Map<String, Object> data;

    public String getDocId() {
        return dockId;
    }

    public Document setDockId(String dockId) {
        this.dockId = dockId;
        return this;
    }

    public String getDsName() {
        return dsName;
    }

    public Document setDsName(String dsName) {
        this.dsName = dsName;
        return this;
    }

    public String getDsType() {
        return dsType;
    }

    public Document setDsType(String dsType) {
        this.dsType = dsType;
        return this;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Document setData(Map<String, Object> data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "Document {" +
                "dockId='" + dockId + '\'' +
                ", dsName='" + dsName + '\'' +
                ", dsType='" + dsType + '\'' +
                ", data=" + data +
                '}';
    }
}
