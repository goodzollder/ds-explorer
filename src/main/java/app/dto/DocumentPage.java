package app.dto;

import java.util.List;


public class DocumentPage {

    private Page page;
    private List<Document> documents;

    public Page getPage() {
        return page;
    }

    public DocumentPage setPage(Page page) {
        this.page = page;
        return this;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public DocumentPage setDocuments(List<Document> documents) {
        this.documents = documents;
        return this;
    }

    @Override
    public String toString() {
        return "DocumentPage{" +
                "page=" + page +
                ", documents=" + documents +
                '}';
    }
}
