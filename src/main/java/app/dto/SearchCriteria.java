package app.dto;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Validated
public class SearchCriteria {
    private String index;
    private String type;
    private String value;
    @Min(value = 0, message = "Page must not be negative")
    private Integer page;
    @Min(value = 0, message = "Size must not be negative")
    private Integer size;

    public String getIndex() {
        return index;
    }

    public SearchCriteria setIndex(String index) {
        this.index = index;
        return this;
    }

    public String getType() {
        return type;
    }

    public SearchCriteria setType(String type) {
        this.type = type;
        return this;
    }

    public String getValue() {
        return value;
    }

    public SearchCriteria setValue(String value) {
        this.value = value;
        return this;
    }

    public Integer getPage() {
        return page;
    }

    public SearchCriteria setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public SearchCriteria setSize(Integer size) {
        this.size = size;
        return this;
    }

    @Override
    public String toString() {
        return "SearchCriteria {" +
                    "index='" + index + '\'' +
                    ", type='" + type + '\'' +
                    ", value='" + value + '\'' +
                    ", page=" + page +
                    ", size=" + size +
                '}';
    }
}
