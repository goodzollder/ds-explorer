package app.dto;

import org.springframework.hateoas.ResourceSupport;

public class Page extends ResourceSupport  {
    private Integer page;
    private Integer size;
    private Long totalDocuments;
    private Long totalPages;

    public Integer getPage() {
        return page;
    }

    public Page setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public Page setSize(Integer size) {
        this.size = size;
        return this;
    }

    public Long getTotalDocuments() {
        return totalDocuments;
    }

    public Page setTotalDocuments(Long totalDocuments) {
        this.totalDocuments = totalDocuments;
        return this;
    }

    public Long getTotalPages() {
        return totalPages;
    }

    public Page setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
        return this;
    }

    @Override
    public String toString() {
        return "Page {" +
                "page=" + page +
                ", size=" + size +
                ", totalDocuments=" + totalDocuments +
                ", totalPages=" + totalPages +
                '}';
    }
}
