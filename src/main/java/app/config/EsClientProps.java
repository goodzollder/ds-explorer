package app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties("es")
@PropertySource("classpath:elasticsearch.properties")
public class EsClientProps {
    @NotBlank
    private String clusterName;
    @NotBlank
    private String host;
    @NotBlank
    private Integer port;

    private Integer connectionTimeout = 10000; // ms

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    @Override
    public String toString() {
        return "EsClientProps{" +
                "host='" + host + '\'' +
                ", port='" + port + '\'' +
                ", clusterName='" + clusterName + '\'' +
                ", connectionTimeout='" + connectionTimeout + '\'' +
                '}';
    }
}
