package app.config;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfig {

    private static final Logger logger = LogManager.getLogger(ApplicationConfig.class);

    @Autowired
    private EsClientProps props;

    @Bean(destroyMethod = "close")
    public RestHighLevelClient esClient() {
        logger.info("Elastic search props {}: ", props.toString());
        HttpHost httpHost = new HttpHost(props.getHost(), props.getPort(), "http");
        return new RestHighLevelClient(RestClient.builder(httpHost));
    }

    @Bean(destroyMethod = "shutdown")
    public ExecutorService postRequestExecutorService() {
        final ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("SearchRequestExecutor-%d")
                .setDaemon(true)
                .build();
        return Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), threadFactory);
    }
}
