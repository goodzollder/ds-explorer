package app.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.rest.RestStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
@RestController
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    private Logger logger = LogManager.getLogger(RestResponseExceptionHandler.class);

    /**
     * Handles {@link ElasticsearchException}.
     * @see <a href="https://github.com/elastic/elasticsearch/issues/30334">Elasticsearch discussion thread</a>
     * @param ex exception to handle
     * @return response entity with HTTP status
     */
    @ExceptionHandler(ElasticsearchException.class)
    public ResponseEntity<ErrorResponse> handleElasticSearchException(ElasticsearchException ex) {
        String details = ex.getSuppressed().length > 0 ? ex.getSuppressed()[0].getMessage() : ex.getDetailedMessage();
        ErrorResponse error = new ErrorResponse("Resource not found", details, ex.status().name());

        if (RestStatus.NOT_FOUND.equals(ex.status())) {
            logger.error("NOT_FOUND exception occurred: data resource {} not found", ex.getResourceId());
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        logger.error("Elasticsearch server exception occurred. {}", details);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ElasticsearchStatusException.class)
    public ResponseEntity<ErrorResponse> handleElasticSearchStatusException(ElasticsearchStatusException ex) {
        logger.error("ElasticsearchStatusException exception occurred.");
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(),
                                                      ex.getDetailedMessage(),
                                                      ex.status().name()),
                                    HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<ErrorResponse> handleElasticSearchIoException(IOException ex) {
        logger.error("Elasticsearch I/O exception occurred.");
        String details = ex.getSuppressed().length > 0 ? ex.getSuppressed()[0].getMessage() : ex.getLocalizedMessage();
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(),
                                                      details,
                                                      HttpStatus.INTERNAL_SERVER_ERROR.name()),
                                    HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArgumentException(IllegalArgumentException ex) {
        logger.error("IllegalArgumentException occurred.");
        String details = ex.getSuppressed().length > 0 ? ex.getSuppressed()[0].getMessage() : ex.toString();
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(),
                                                      details,
                                                      HttpStatus.BAD_REQUEST.name()),
                                    HttpStatus.BAD_REQUEST);
    }
}
