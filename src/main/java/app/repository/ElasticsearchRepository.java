package app.repository;

import app.dto.SearchCriteria;
import org.apache.http.client.methods.HttpGet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
public class ElasticsearchRepository {
    private Logger logger = LogManager.getLogger(ElasticsearchRepository.class);

    private final RestHighLevelClient client;

    @Autowired
    public ElasticsearchRepository(RestHighLevelClient client) {
        this.client = client;
    }

    public Response getClusterInfo() throws IOException {
        logger.debug("Retrieving cluster info.");

        Request request = new Request(HttpGet.METHOD_NAME, "/");
        return client.getLowLevelClient().performRequest(request);
    }

    public Response getAllIndices() throws IOException {
        logger.debug("Retrieving existing indices.");

        Request request = new Request(HttpGet.METHOD_NAME, "/_cat/indices?v&format=json");
        return client.getLowLevelClient().performRequest(request);
    }

    public SearchResponse search(SearchCriteria criteria) throws IOException, ElasticsearchException {
        logger.debug("Elasticsearch search criteria {}", criteria.toString());

        SearchRequest searchRequest = new SearchRequest();  // without arguments searches all indices
        if (Strings.hasText(criteria.getIndex())) {
            searchRequest.indices(criteria.getIndex());
        }
        if(Strings.hasText(criteria.getType())) {
            searchRequest.types(criteria.getType());
        }

        QueryBuilder query = Strings.hasText(criteria.getValue()) ?
                QueryBuilders.queryStringQuery(criteria.getValue()) :
                QueryBuilders.matchAllQuery();

        int offset = criteria.getPage() * criteria.getSize();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().query(query)
                                                                           .from(offset)
                                                                           .size(criteria.getSize());
        return client.search(searchRequest.source(searchSourceBuilder), RequestOptions.DEFAULT);
    }
}
