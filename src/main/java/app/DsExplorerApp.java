package app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("app")
public class DsExplorerApp {

    private static final Logger logger = LogManager.getLogger(DsExplorerApp.class);

    public static void main(String[] args) {
        logger.info("Starting DS Explorer application.");
        SpringApplication.run(DsExplorerApp.class, args);
    }
}
